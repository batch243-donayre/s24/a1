// Cube
let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);


// Address
const address = [street ='258 Washingtone Ave NW',
				 city = 'California',
				 zipCode = '90011'
	]
console.log(`I live at ${street}, ${city} ${zipCode}`);


// Animals
const animal ={
	name: "Lolong",
	species: "salwater crocodile",
	weight: "187 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);


// ForEach
const reduceNumber = [1,2,3,4,5];
let arraySum = 0;

reduceNumber.forEach(reduceNumber =>{
	console.log(reduceNumber);
	arraySum +=reduceNumber;
})

console.log(arraySum);


// Dog
class Dog{
		constructor(name, age, breed){
			this.dogName = name;
			this.dogAge = age;
			this.dogBreed = breed;
		}
	}

let	dog = new Dog('Frankie', 5, 'Maniature Dachshund');
console.log(dog);